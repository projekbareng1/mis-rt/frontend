// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    "@nuxtjs/tailwindcss",
    "nuxt-icon",
    [
      "@pinia/nuxt",
      {
        autoImports: ["defineStore", "acceptHMRUpdate"],
      },
    ],
  ],
  routeRules: {
    "/": { redirect: "/application" },
  },
  pinia: {
    disableVuex: true,
  },
  imports: {
    dirs: ["stores"],
  },
  runtimeConfig: {
    public: {
      apiUrl: process.env.API_URL,
    },
  },
  plugins: ["~/plugins/axios.ts"],
  // nitro: {
  //   preset: "vercel",
  // },

  // ssr: false,
});
