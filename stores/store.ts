// no need to import defineStore and acceptHMRUpdate
export const useToastStore = defineStore("toast", {
  state: () => ({
    message: "",
    type: "",
    visible: false,
    visibleTime: 4000,
    userData: null,
  }),
  getters: {
    isVisible: (state) => state.visible,
  },
  actions: {
    showToast(message: string, type: string) {
      this.message = message;
      this.type = type;
      this.visible = true;
      setTimeout(() => {
        this.message = "";
        this.type = "";
        this.visible = false;
      }, this.visibleTime);
    },
  },
});

export const useUserDataStore = defineStore("user_data", {
  state: () => ({
    email: "",
    id: "",
    name: "",
    role: "",
    username: "",
  }),
  getters: {
    getRole: (state) => state.role,
    userId: (state) => state.id,
    getUser: (state) => {
      return {
        email: state.email,
        id: state.id,
        name: state.name,
        role: state.role,
        username: state.username,
      };
    },
  },
  actions: {
    updateUserData(data: any) {
      console.log(data.role.name);
      this.email = data.email;
      this.id = data.id;
      this.name = data.name;
      this.username = data.username;
      this.role = data.role.name;
    },
  },
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useToastStore, import.meta.hot));
}
