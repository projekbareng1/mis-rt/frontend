# Use an official Node.js runtime as the base image
FROM node:18.6.0-alpine3.15

# Set the working directory in the container
WORKDIR /app

# Copy package.json and package-lock.json to the container
COPY package*.json ./

# Install project dependencies
RUN npm install

# Copy the rest of your Nuxt.js app to the container
COPY . .

# Build your Nuxt.js app
RUN npm run build

# Expose the port the app runs on (default is 3000)
EXPOSE 3000

# Start the Nuxt.js app
CMD ["node", ".output/server/index.mjs"]
