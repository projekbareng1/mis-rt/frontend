import { _ as _export_sfc, u as useRuntimeConfig, a as __nuxt_component_0 } from '../server.mjs';
import { _ as __nuxt_component_0$1 } from './index-094067bd.mjs';
import { _ as __nuxt_component_0$2 } from './nuxt-link-faa5b7b9.mjs';
import __nuxt_component_0$3 from './Icon-e9878bea.mjs';
import { useSSRContext, mergeProps, withCtx, createVNode } from 'vue';
import { ssrRenderComponent, ssrRenderAttrs, ssrInterpolate, ssrRenderList, ssrRenderClass, ssrIncludeBooleanAttr } from 'vue/server-renderer';
import axios from 'axios';
import '../../nitro/vercel.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import 'unhead';
import '@unhead/shared';
import 'vue-router';
import './config-7450675c.mjs';
import '@iconify/vue/dist/offline';
import '@iconify/vue';

const _sfc_main$1 = {
  props: {
    tableName: String,
    addPath: String,
    currentPage: Number,
    totalPages: Number,
    heads: Array,
    items: Array
  },
  data() {
    return {};
  },
  methods: {
    prevPage() {
      if (this.currentPage > 1) {
        this.$emit("page-change", this.currentPage - 1);
      }
    },
    nextPage() {
      if (this.currentPage < this.totalPages) {
        this.$emit("page-change", this.currentPage + 1);
      }
    }
  }
};
function _sfc_ssrRender$1(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_NuxtLink = __nuxt_component_0$2;
  const _component_Icon = __nuxt_component_0$3;
  _push(`<div${ssrRenderAttrs(_attrs)}><h2 class="text-center text-2xl uppercase tracking-widest mb-6">${ssrInterpolate($props.tableName)}</h2><div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg"><table class="min-w-full"><thead class="bg-gray-200"><tr><!--[-->`);
  ssrRenderList($props.heads, (head) => {
    _push(`<th class="text-left px-2 py-1 text-gray-600 uppercase tracking-wider">${ssrInterpolate(head)}</th>`);
  });
  _push(`<!--]--></tr></thead><tbody class="bg-white"><!--[-->`);
  ssrRenderList($props.items, (item) => {
    _push(`<tr class="${ssrRenderClass({ "bg-gray-100": item.id % 2 === 0 })}"><!--[-->`);
    ssrRenderList(item, (cell) => {
      _push(`<!--[-->`);
      if (typeof cell !== typeof {}) {
        _push(`<td class="px-2 py-2 whitespace-no-wrap">${ssrInterpolate(cell)}</td>`);
      } else {
        _push(`<td class="flex"><!--[-->`);
        ssrRenderList(cell, (btn) => {
          _push(`<!--[-->`);
          if (btn.route) {
            _push(ssrRenderComponent(_component_NuxtLink, {
              href: btn.route,
              class: [`${btn.color} ${btn.disabled ? "hidden" : ""}`, "text-white my-1 mx-1 p-2 rounded hover:cursor-pointer"]
            }, {
              default: withCtx((_, _push2, _parent2, _scopeId) => {
                if (_push2) {
                  _push2(ssrRenderComponent(_component_Icon, {
                    name: btn.icon
                  }, null, _parent2, _scopeId));
                } else {
                  return [
                    createVNode(_component_Icon, {
                      name: btn.icon
                    }, null, 8, ["name"])
                  ];
                }
              }),
              _: 2
            }, _parent));
          } else {
            _push(ssrRenderComponent(_component_NuxtLink, {
              onClick: ($event) => _ctx.$emit(btn.callback, item.id),
              class: [`${btn.color} ${btn.disabled ? "hidden" : ""}`, "text-white my-1 mx-1 p-2 rounded hover:cursor-pointer"]
            }, {
              default: withCtx((_, _push2, _parent2, _scopeId) => {
                if (_push2) {
                  _push2(ssrRenderComponent(_component_Icon, {
                    name: btn.icon
                  }, null, _parent2, _scopeId));
                } else {
                  return [
                    createVNode(_component_Icon, {
                      name: btn.icon
                    }, null, 8, ["name"])
                  ];
                }
              }),
              _: 2
            }, _parent));
          }
          _push(`<!--]-->`);
        });
        _push(`<!--]--></td>`);
      }
      _push(`<!--]-->`);
    });
    _push(`<!--]--></tr>`);
  });
  _push(`<!--]--></tbody></table><div class="flex w-full justify-between"><div class="flex items-center justify-left space-x-4 m-4"><button${ssrIncludeBooleanAttr($props.currentPage === 1) ? " disabled" : ""} class="bg-blue-500 text-white px-3 py-2 rounded disabled:opacity-50">`);
  _push(ssrRenderComponent(_component_Icon, {
    class: "w-full h-full",
    name: "material-symbols:skip-previous-rounded"
  }, null, _parent));
  _push(`</button><span class="text-lg text-gray-600 uppercase tracking-wider">${ssrInterpolate($props.currentPage)}</span><button${ssrIncludeBooleanAttr($props.currentPage === $props.totalPages) ? " disabled" : ""} class="bg-blue-500 text-white px-3 py-2 rounded disabled:opacity-50">`);
  _push(ssrRenderComponent(_component_Icon, {
    class: "w-full h-full",
    name: "material-symbols:skip-next-rounded"
  }, null, _parent));
  _push(`</button></div><div class="flex items-center justify-right space-x-4 m-4">`);
  _push(ssrRenderComponent(_component_NuxtLink, {
    onClick: ($event) => _ctx.$router.push($props.addPath),
    class: "bg-blue-500 text-white px-3 py-2 rounded disabled:opacity-50 hover:cursor-pointer"
  }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(ssrRenderComponent(_component_Icon, {
          class: "w-full h-full",
          name: "material-symbols:add-box-rounded"
        }, null, _parent2, _scopeId));
      } else {
        return [
          createVNode(_component_Icon, {
            class: "w-full h-full",
            name: "material-symbols:add-box-rounded"
          })
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`</div></div></div></div>`);
}
const _sfc_setup$1 = _sfc_main$1.setup;
_sfc_main$1.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/CustomTable/index.vue");
  return _sfc_setup$1 ? _sfc_setup$1(props, ctx) : void 0;
};
const __nuxt_component_2 = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["ssrRender", _sfc_ssrRender$1]]);
const _sfc_main = {
  layout: "dashboard",
  data() {
    return {
      toastVisible: false,
      toastTitle: "",
      toastMessage: "",
      tableName: "Files",
      currentPage: 1,
      totalPages: 1,
      heads: [
        "No",
        "Title",
        "Description",
        "Owner Name",
        "Owner Email",
        "Action"
      ],
      files: []
    };
  },
  mounted() {
    this.fetchData();
  },
  methods: {
    fetchData() {
      const config = /* @__PURE__ */ useRuntimeConfig();
      axios.get(`${config.public.apiUrl}/file/`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`
        }
      }).then((response) => {
        let datas = response.data;
        console.log(datas);
        datas.map((item) => {
          item.owner_name = item.owner.name;
          item.owner_email = item.owner.email;
          delete item.owner;
          item.action = [
            {
              icon: "material-symbols:edit-outline",
              color: "bg-blue-500",
              route: `/upload/${item.id}`,
              disabled: false
            },
            {
              icon: "material-symbols:cloud-download-outline-rounded",
              color: "bg-green-500",
              route: `/files/${item.id}`,
              disabled: item.file_path ? false : true
            },
            {
              icon: "material-symbols:delete-outline-rounded",
              color: "bg-red-500",
              disabled: false,
              callback: "delete-file"
            }
          ];
          const filepath = item.file_path ? item.file_path : "";
          delete item.file_path;
          item.file_path = filepath;
        });
        this.files = datas;
        console.log(datas);
      }).catch((error) => {
        console.log(error);
        const detail = error.response.data.detail;
        let message = "";
        if (detail[0].msg) {
          message = detail[0].msg;
        } else {
          message = detail;
        }
        this.showToast("error", message);
      });
    },
    addFile() {
    },
    editFile(data) {
      console.log(data);
    },
    deleteFile(id) {
      console.log(id);
    },
    showToast(title, message) {
      this.toastTitle = title;
      this.toastMessage = message;
      this.toastVisible = true;
      setTimeout(() => {
        this.toastVisible = false;
      }, 2e3);
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_NuxtLayout = __nuxt_component_0;
  const _component_Toast = __nuxt_component_0$1;
  const _component_CustomTable = __nuxt_component_2;
  _push(ssrRenderComponent(_component_NuxtLayout, mergeProps({ name: "dashboard" }, _attrs), {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`<div class="container p-8"${_scopeId}>`);
        _push2(ssrRenderComponent(_component_Toast, {
          visible: $data.toastVisible,
          title: $data.toastTitle,
          message: $data.toastMessage
        }, null, _parent2, _scopeId));
        _push2(ssrRenderComponent(_component_CustomTable, {
          tableName: $data.tableName,
          currentPage: $data.currentPage,
          totalPages: $data.totalPages,
          heads: $data.heads,
          items: $data.files,
          addPath: "/upload",
          onDeleteFile: $options.deleteFile
        }, null, _parent2, _scopeId));
        _push2(`</div>`);
      } else {
        return [
          createVNode("div", { class: "container p-8" }, [
            createVNode(_component_Toast, {
              visible: $data.toastVisible,
              title: $data.toastTitle,
              message: $data.toastMessage
            }, null, 8, ["visible", "title", "message"]),
            createVNode(_component_CustomTable, {
              tableName: $data.tableName,
              currentPage: $data.currentPage,
              totalPages: $data.totalPages,
              heads: $data.heads,
              items: $data.files,
              addPath: "/upload",
              onDeleteFile: $options.deleteFile
            }, null, 8, ["tableName", "currentPage", "totalPages", "heads", "items", "onDeleteFile"])
          ])
        ];
      }
    }),
    _: 1
  }, _parent));
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/files/index.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const index = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);

export { index as default };
//# sourceMappingURL=index-93667afe.mjs.map
