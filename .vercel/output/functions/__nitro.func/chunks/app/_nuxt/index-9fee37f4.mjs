import { _ as _export_sfc, u as useRuntimeConfig, a as __nuxt_component_0 } from '../server.mjs';
import { _ as __nuxt_component_0$1 } from './index-094067bd.mjs';
import { _ as __nuxt_component_2 } from './index-aa053c9c.mjs';
import { mergeProps, withCtx, createVNode, useSSRContext } from 'vue';
import axios from 'axios';
import { ssrRenderComponent } from 'vue/server-renderer';
import '../../nitro/vercel.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import 'unhead';
import '@unhead/shared';
import 'vue-router';
import './Icon-e9878bea.mjs';
import './config-7450675c.mjs';
import '@iconify/vue/dist/offline';
import '@iconify/vue';

const _sfc_main = {
  data() {
    return {
      toastVisible: false,
      toastTitle: "",
      toastMessage: "",
      formTitle: "Upload PDF Baru",
      submitName: "Save",
      fields: [
        { id: "title", name: "Title", type: "title", value: "" },
        {
          id: "description",
          name: "Description",
          type: "description",
          value: ""
        }
      ]
    };
  },
  methods: {
    onSubmit(data) {
      const config = /* @__PURE__ */ useRuntimeConfig();
      const userDataString = localStorage.getItem("user_data");
      const accessToken = localStorage.getItem("access_token");
      JSON.parse(userDataString);
      axios.post(`${config.public.apiUrl}/file/`, data, {
        headers: {
          Authorization: `Bearer ${accessToken}`
        }
      }).then((response) => {
        console.log(response);
        this.showToast("success", "Data stored successfully");
        this.$router.replace("/files");
      }).catch((error) => {
        console.log(error);
        const detail = error.response.data.detail;
        let message = "";
        if (detail[0].msg) {
          message = detail[0].msg;
        } else {
          message = detail;
        }
        this.showToast("error", message);
      });
    },
    showToast(title, message) {
      this.toastTitle = title;
      this.toastMessage = message;
      this.toastVisible = true;
      setTimeout(() => {
        this.toastVisible = false;
      }, 2e3);
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_NuxtLayout = __nuxt_component_0;
  const _component_Toast = __nuxt_component_0$1;
  const _component_DynamicForm = __nuxt_component_2;
  _push(ssrRenderComponent(_component_NuxtLayout, mergeProps({ name: "dashboard" }, _attrs), {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`<div class="flex justify-evenly container p-8"${_scopeId}>`);
        _push2(ssrRenderComponent(_component_Toast, {
          visible: $data.toastVisible,
          title: $data.toastTitle,
          message: $data.toastMessage
        }, null, _parent2, _scopeId));
        _push2(`<div class="p-2 my-12 rounded-lg lg:w-1/3 xl:1/2 h-fit border-solid border-slate-300 border-2"${_scopeId}>`);
        _push2(ssrRenderComponent(_component_DynamicForm, {
          fields: $data.fields,
          formTitle: "PDF Metadata",
          submitName: $data.submitName,
          onSubmit: $options.onSubmit
        }, null, _parent2, _scopeId));
        _push2(`</div><div class="p-2 my-12 rounded-lg lg:w-1/3 xl:1/2 h-fit border-solid border-slate-300 border-2"${_scopeId}>`);
        _push2(ssrRenderComponent(_component_DynamicForm, {
          fields: $data.fields,
          formTitle: "Upload file PDF",
          submitName: $data.submitName,
          onSubmit: $options.onSubmit
        }, null, _parent2, _scopeId));
        _push2(`</div></div>`);
      } else {
        return [
          createVNode("div", { class: "flex justify-evenly container p-8" }, [
            createVNode(_component_Toast, {
              visible: $data.toastVisible,
              title: $data.toastTitle,
              message: $data.toastMessage
            }, null, 8, ["visible", "title", "message"]),
            createVNode("div", { class: "p-2 my-12 rounded-lg lg:w-1/3 xl:1/2 h-fit border-solid border-slate-300 border-2" }, [
              createVNode(_component_DynamicForm, {
                fields: $data.fields,
                formTitle: "PDF Metadata",
                submitName: $data.submitName,
                onSubmit: $options.onSubmit
              }, null, 8, ["fields", "submitName", "onSubmit"])
            ]),
            createVNode("div", { class: "p-2 my-12 rounded-lg lg:w-1/3 xl:1/2 h-fit border-solid border-slate-300 border-2" }, [
              createVNode(_component_DynamicForm, {
                fields: $data.fields,
                formTitle: "Upload file PDF",
                submitName: $data.submitName,
                onSubmit: $options.onSubmit
              }, null, 8, ["fields", "submitName", "onSubmit"])
            ])
          ])
        ];
      }
    }),
    _: 1
  }, _parent));
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/upload/[id]/index.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const index = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);

export { index as default };
//# sourceMappingURL=index-9fee37f4.mjs.map
