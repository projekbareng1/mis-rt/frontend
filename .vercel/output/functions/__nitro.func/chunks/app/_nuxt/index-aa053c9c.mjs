import { mergeProps, useSSRContext } from 'vue';
import { ssrRenderAttrs, ssrInterpolate, ssrRenderList, ssrRenderAttr, ssrRenderDynamicModel } from 'vue/server-renderer';
import { _ as _export_sfc } from '../server.mjs';

const _sfc_main = {
  props: {
    formTitle: String,
    submitName: String,
    fields: Array,
    onSubmit: Function
  },
  data() {
    return {};
  },
  methods: {
    handleSubmit() {
      const form = this.fields.reduce((result, current) => {
        result[current.id] = current.value;
        return result;
      }, {});
      if (typeof this.onSubmit === "function") {
        this.onSubmit(form);
      }
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  _push(`<form${ssrRenderAttrs(mergeProps({ class: "p-2" }, _attrs))}><h2 class="w-full text-center mb-4 text-xl font-semibold">${ssrInterpolate($props.formTitle)}</h2><!--[-->`);
  ssrRenderList($props.fields, (field, index) => {
    _push(`<div><div class="mb-4"><label${ssrRenderAttr("for", field.id)} class="block text-sm font-medium text-gray-700">${ssrInterpolate(field.name)}</label><input${ssrRenderAttr("type", field.type)}${ssrRenderAttr("id", field.id)}${ssrRenderAttr("name", field.id)}${ssrRenderDynamicModel(field.type, field.value, null)} class="w-full px-4 py-2 border rounded-lg focus:outline-none focus:ring focus:border-blue-300"${ssrRenderAttr("placeholder", `Enter your ${field.name}`)}></div></div>`);
  });
  _push(`<!--]--><button type="submit" class="w-full py-2 px-4 mt-4 bg-blue-500 text-white rounded-lg hover:bg-blue-600">${ssrInterpolate($props.submitName)}</button></form>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/DynamicForm/index.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const __nuxt_component_2 = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);

export { __nuxt_component_2 as _ };
//# sourceMappingURL=index-aa053c9c.mjs.map
