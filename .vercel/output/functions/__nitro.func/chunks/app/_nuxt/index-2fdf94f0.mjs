import { _ as __nuxt_component_0 } from './index-094067bd.mjs';
import { _ as _export_sfc, a as __nuxt_component_0$1 } from '../server.mjs';
import { _ as __nuxt_component_0$2 } from './nuxt-link-faa5b7b9.mjs';
import { useSSRContext, withCtx, createVNode, mergeProps, toDisplayString } from 'vue';
import { ssrRenderComponent, ssrRenderAttrs, ssrRenderList, ssrInterpolate } from 'vue/server-renderer';
import './Icon-e9878bea.mjs';
import './config-7450675c.mjs';
import '../../nitro/vercel.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import '@iconify/vue/dist/offline';
import '@iconify/vue';
import 'unhead';
import '@unhead/shared';
import 'vue-router';
import 'axios';

const _sfc_main$1 = {
  props: {
    items: Array
  }
};
function _sfc_ssrRender$1(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_NuxtLink = __nuxt_component_0$2;
  _push(`<div${ssrRenderAttrs(mergeProps({ class: "p-8 space-y-2" }, _attrs))}><!--[-->`);
  ssrRenderList($props.items, (item) => {
    _push(`<div class="max-w-md mx-auto bg-white rounded-xl shadow-md overflow-hidden">`);
    _push(ssrRenderComponent(_component_NuxtLink, {
      href: item.file_path
    }, {
      default: withCtx((_, _push2, _parent2, _scopeId) => {
        if (_push2) {
          _push2(`<div class="md:flex hover:cursor-pointer"${_scopeId}><div class="md:flex-shrink-0"${_scopeId}><img class="h-48 w-full object-cover md:w-48" src="https://via.placeholder.com/150" alt="Card image"${_scopeId}></div><div class="p-8"${_scopeId}><div class="uppercase tracking-wide text-sm text-indigo-500 font-semibold"${_scopeId}> Category </div><span class="block mt-1 text-lg leading-tight font-medium text-black hover:underline"${_scopeId}>${ssrInterpolate(item.title)}</span><p class="mt-2 text-gray-500"${_scopeId}>${ssrInterpolate(item.description)}</p></div></div>`);
        } else {
          return [
            createVNode("div", { class: "md:flex hover:cursor-pointer" }, [
              createVNode("div", { class: "md:flex-shrink-0" }, [
                createVNode("img", {
                  class: "h-48 w-full object-cover md:w-48",
                  src: "https://via.placeholder.com/150",
                  alt: "Card image"
                })
              ]),
              createVNode("div", { class: "p-8" }, [
                createVNode("div", { class: "uppercase tracking-wide text-sm text-indigo-500 font-semibold" }, " Category "),
                createVNode("span", { class: "block mt-1 text-lg leading-tight font-medium text-black hover:underline" }, toDisplayString(item.title), 1),
                createVNode("p", { class: "mt-2 text-gray-500" }, toDisplayString(item.description), 1)
              ])
            ])
          ];
        }
      }),
      _: 2
    }, _parent));
    _push(`</div>`);
  });
  _push(`<!--]--></div>`);
}
const _sfc_setup$1 = _sfc_main$1.setup;
_sfc_main$1.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/CardList/index.vue");
  return _sfc_setup$1 ? _sfc_setup$1(props, ctx) : void 0;
};
const __nuxt_component_2 = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["ssrRender", _sfc_ssrRender$1]]);
const _sfc_main = {
  middleware: ["auth"],
  data() {
    return {
      toastVisible: false,
      toastTitle: "",
      toastMessage: "",
      cards: [{}]
    };
  },
  mounted() {
    this.fetchData();
  },
  methods: {
    fetchData() {
      this.$api.get(`/file/`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem("access_token")}`,
          Accept: "application/json"
        }
      }).then((response) => {
        let datas = response.data;
        console.log(datas);
        datas.map((item) => {
          item.owner_name = item.owner.name;
          item.owner_email = item.owner.email;
          delete item.owner;
          item.action = [
            {
              icon: "material-symbols:edit-outline",
              color: "bg-blue-500",
              route: `/upload/${item.id}`,
              disabled: false
            },
            {
              icon: "material-symbols:cloud-download-outline-rounded",
              color: "bg-green-500",
              route: `/files/${item.id}`,
              disabled: item.file_path ? false : true
            },
            {
              icon: "material-symbols:delete-outline-rounded",
              color: "bg-red-500",
              disabled: false,
              callback: "delete-file"
            }
          ];
          const filepath = item.file_path ? item.file_path : "";
          delete item.file_path;
          item.file_path = filepath;
        });
        this.cards = datas;
        console.log(datas);
      }).catch((error) => {
        console.log(error);
        const detail = error.response.data.detail;
        let message = "";
        if (detail[0].msg) {
          message = detail[0].msg;
        } else {
          message = detail;
        }
        this.showToast("error", message);
      });
    },
    showToast(title, message) {
      this.toastTitle = title;
      this.toastMessage = message;
      this.toastVisible = true;
      setTimeout(() => {
        this.toastVisible = false;
      }, 2e3);
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Toast = __nuxt_component_0;
  const _component_NuxtLayout = __nuxt_component_0$1;
  const _component_CardList = __nuxt_component_2;
  _push(`<!--[-->`);
  _push(ssrRenderComponent(_component_Toast, {
    visible: $data.toastVisible,
    title: $data.toastTitle,
    message: $data.toastMessage
  }, null, _parent));
  _push(ssrRenderComponent(_component_NuxtLayout, { name: "dashboard" }, {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(ssrRenderComponent(_component_CardList, { items: $data.cards }, null, _parent2, _scopeId));
      } else {
        return [
          createVNode(_component_CardList, { items: $data.cards }, null, 8, ["items"])
        ];
      }
    }),
    _: 1
  }, _parent));
  _push(`<!--]-->`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/dashboard/index.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const index = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);

export { index as default };
//# sourceMappingURL=index-2fdf94f0.mjs.map
