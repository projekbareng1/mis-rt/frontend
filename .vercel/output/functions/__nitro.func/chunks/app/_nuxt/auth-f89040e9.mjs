import { d as defineNuxtRouteMiddleware, e as executeAsync, n as navigateTo } from '../server.mjs';
import 'vue';
import '../../nitro/vercel.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import 'unhead';
import '@unhead/shared';
import 'vue-router';
import 'axios';
import 'vue/server-renderer';

const isAuthenticated = async () => {
  return true;
};
const auth = /* @__PURE__ */ defineNuxtRouteMiddleware(async (to, from) => {
  let __temp, __restore;
  const status = ([__temp, __restore] = executeAsync(() => isAuthenticated()), __temp = await __temp, __restore(), __temp);
  if (status === false) {
    console.log("status", status);
    return navigateTo("/login");
  }
});

export { auth as default };
//# sourceMappingURL=auth-f89040e9.mjs.map
