import { _ as __nuxt_component_0 } from './index-094067bd.mjs';
import __nuxt_component_0$1 from './Icon-e9878bea.mjs';
import { useSSRContext, mergeProps, withCtx, createVNode, toDisplayString } from 'vue';
import { ssrRenderAttrs, ssrRenderComponent, ssrRenderSlot, ssrRenderClass, ssrRenderList, ssrInterpolate } from 'vue/server-renderer';
import { _ as _export_sfc } from '../server.mjs';
import { _ as __nuxt_component_0$2 } from './nuxt-link-faa5b7b9.mjs';
import { u as useToastStore } from './store-946753a9.mjs';
import './config-7450675c.mjs';
import '../../nitro/vercel.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import '@iconify/vue/dist/offline';
import '@iconify/vue';
import 'unhead';
import '@unhead/shared';
import 'vue-router';
import 'axios';

const _sfc_main$2 = {
  methods: {
    logout() {
      localStorage.removeItem("access_token");
      localStorage.removeItem("user_data");
      this.$router.push("/login");
    }
  }
};
function _sfc_ssrRender$2(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Icon = __nuxt_component_0$1;
  _push(`<header${ssrRenderAttrs(mergeProps({ class: "w-full bg-blue-500 py-4 px-8" }, _attrs))}><div class="flex mx-auto items-center justify-between"><a class="text-white text-xl font-bold" href="/">Your Logo</a><nav></nav><button class="bg-red-500 text-white px-4 py-2 rounded hover:bg-red-600">`);
  _push(ssrRenderComponent(_component_Icon, { name: "octicon:sign-out" }, null, _parent));
  _push(` Logout </button></div></header>`);
}
const _sfc_setup$2 = _sfc_main$2.setup;
_sfc_main$2.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/Header/index.vue");
  return _sfc_setup$2 ? _sfc_setup$2(props, ctx) : void 0;
};
const __nuxt_component_1 = /* @__PURE__ */ _export_sfc(_sfc_main$2, [["ssrRender", _sfc_ssrRender$2]]);
const _sfc_main$1 = {
  props: {
    items: Array
  },
  data() {
    return {
      sidebarVisible: true
    };
  },
  methods: {
    toggleSidebar() {
      this.sidebarVisible = !this.sidebarVisible;
      console.log(this.sidebarVisible);
    },
    isSelected(path) {
      let classVal = "";
      const firstPathSegment = this.$route.path.split("/")[1];
      if (firstPathSegment == path.replace("/", "")) {
        classVal = this.sidebarVisible ? "bg-slate-500 justify-start py-1" : "bg-slate-500 justify-center py-3";
      } else {
        classVal = this.sidebarVisible ? "bg-transparent justify-start py-1" : "bg-transparent justify-center py-3";
      }
      return classVal;
    }
  },
  computed: {
    sidebarWidth() {
      return this.sidebarVisible ? "w-64" : "w-16";
    }
  }
};
function _sfc_ssrRender$1(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Icon = __nuxt_component_0$1;
  const _component_nuxt_link = __nuxt_component_0$2;
  _push(`<aside${ssrRenderAttrs(mergeProps({
    class: ["bg-gray-800 min-h-screen my-0 shadow-xl", $options.sidebarWidth]
  }, _attrs))}><div class="flex flex-col p-2 mb-4"><div class="${ssrRenderClass([$data.sidebarVisible ? "justify-end" : "justify-center", "flex mb-4"])}"><button class="text-white hover:text-gray-300 px-2 py-1 mt-4">`);
  _push(ssrRenderComponent(_component_Icon, {
    size: "25",
    class: "w-full h-full",
    name: "uis:bars"
  }, null, _parent));
  _push(`</button></div><ul class="${ssrRenderClass("space-y-2")}"><!--[-->`);
  ssrRenderList($props.items, (item) => {
    _push(`<li class="flex my-0">`);
    _push(ssrRenderComponent(_component_nuxt_link, {
      to: item.path,
      class: [$options.isSelected(item.path), "flex items-center w-full text-white rounded-lg text-center px-2 my-0 hover:text-gray-300 hover:bg-slate-500 active:bg-slate-600"]
    }, {
      default: withCtx((_, _push2, _parent2, _scopeId) => {
        if (_push2) {
          _push2(ssrRenderComponent(_component_Icon, {
            size: "25",
            name: item.icon
          }, null, _parent2, _scopeId));
          _push2(`<span class="${ssrRenderClass([$data.sidebarVisible ? "" : "hidden", "ml-2"])}"${_scopeId}>${ssrInterpolate(item.title)}</span>`);
        } else {
          return [
            createVNode(_component_Icon, {
              size: "25",
              name: item.icon
            }, null, 8, ["name"]),
            createVNode("span", {
              class: ["ml-2", $data.sidebarVisible ? "" : "hidden"]
            }, toDisplayString(item.title), 3)
          ];
        }
      }),
      _: 2
    }, _parent));
    _push(`</li>`);
  });
  _push(`<!--]--></ul></div></aside>`);
}
const _sfc_setup$1 = _sfc_main$1.setup;
_sfc_main$1.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/Sidebar/index.vue");
  return _sfc_setup$1 ? _sfc_setup$1(props, ctx) : void 0;
};
const __nuxt_component_2 = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["ssrRender", _sfc_ssrRender$1]]);
const _sfc_main = {
  data() {
    return {
      toastVisible: false,
      toastTitle: "",
      toastMessage: "",
      items: [
        {
          title: "Dashboard",
          path: "/dashboard",
          icon: "ic:outline-dashboard"
        },
        {
          title: "Profile",
          path: "/profile",
          icon: "carbon:user-profile"
        },
        {
          title: "File Manager",
          path: "/files",
          icon: "material-symbols-light:bookmark-manager-rounded"
        },
        {
          title: "File Upload",
          path: "/upload",
          icon: "bxs:file"
        }
        // {
        //   title: "Take Picture",
        //   path: "/capture",
        //   icon: "material-symbols:android-camera-outline",
        // },
      ]
    };
  },
  methods: {
    showToast(title, message) {
      const store = useToastStore();
      console.log(store.isVisible);
      this.toastTitle = title;
      this.toastMessage = message;
      this.toastVisible = true;
      setTimeout(() => {
        this.toastVisible = false;
      }, 2e3);
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Toast = __nuxt_component_0;
  const _component_Header = __nuxt_component_1;
  const _component_Sidebar = __nuxt_component_2;
  _push(`<div${ssrRenderAttrs(mergeProps({ class: "flex flex-col w-full" }, _attrs))}>`);
  _push(ssrRenderComponent(_component_Toast, {
    visible: $data.toastVisible,
    title: $data.toastTitle,
    message: $data.toastMessage
  }, null, _parent));
  _push(ssrRenderComponent(_component_Header, null, null, _parent));
  _push(`<div class="flex">`);
  _push(ssrRenderComponent(_component_Sidebar, { items: $data.items }, null, _parent));
  ssrRenderSlot(_ctx.$slots, "default", { class: "bg-slate-500" }, null, _push, _parent);
  _push(`</div></div>`);
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("layouts/dashboard.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const dashboard = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);

export { dashboard as default };
//# sourceMappingURL=dashboard-148277c3.mjs.map
