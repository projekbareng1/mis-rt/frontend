const client_manifest = {
  "_index.3b6688e6.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "index.3b6688e6.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_index.aa7e97b8.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "index.aa7e97b8.js",
    "imports": [
      "node_modules/nuxt-icon/dist/runtime/Icon.vue",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_nuxt-link.83d46d79.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "nuxt-link.83d46d79.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_store.843989b0.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "store.843989b0.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_vue.f36acd1f.f52f7405.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "vue.f36acd1f.f52f7405.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "layouts/auth.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "auth.e41f7ad0.js",
    "imports": [
      "_index.aa7e97b8.js",
      "_store.843989b0.js",
      "node_modules/nuxt/dist/app/entry.js",
      "node_modules/nuxt-icon/dist/runtime/Icon.vue"
    ],
    "isDynamicEntry": true,
    "src": "layouts/auth.vue"
  },
  "layouts/dashboard.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "dashboard.cc4b7625.js",
    "imports": [
      "_index.aa7e97b8.js",
      "node_modules/nuxt-icon/dist/runtime/Icon.vue",
      "node_modules/nuxt/dist/app/entry.js",
      "_nuxt-link.83d46d79.js",
      "_store.843989b0.js"
    ],
    "isDynamicEntry": true,
    "src": "layouts/dashboard.vue"
  },
  "middleware/auth.ts": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "auth.5b3102a2.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "isDynamicEntry": true,
    "src": "middleware/auth.ts"
  },
  "node_modules/@nuxt/ui-templates/dist/templates/error-404.css": {
    "resourceType": "style",
    "prefetch": true,
    "preload": true,
    "file": "error-404.95c28eb4.css",
    "src": "node_modules/@nuxt/ui-templates/dist/templates/error-404.css"
  },
  "node_modules/@nuxt/ui-templates/dist/templates/error-404.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "css": [],
    "file": "error-404.d7d2b056.js",
    "imports": [
      "_nuxt-link.83d46d79.js",
      "_vue.f36acd1f.f52f7405.js",
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "isDynamicEntry": true,
    "src": "node_modules/@nuxt/ui-templates/dist/templates/error-404.vue"
  },
  "error-404.95c28eb4.css": {
    "file": "error-404.95c28eb4.css",
    "resourceType": "style",
    "prefetch": true,
    "preload": true
  },
  "node_modules/@nuxt/ui-templates/dist/templates/error-500.css": {
    "resourceType": "style",
    "prefetch": true,
    "preload": true,
    "file": "error-500.e798523c.css",
    "src": "node_modules/@nuxt/ui-templates/dist/templates/error-500.css"
  },
  "node_modules/@nuxt/ui-templates/dist/templates/error-500.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "css": [],
    "file": "error-500.e075f3d4.js",
    "imports": [
      "_vue.f36acd1f.f52f7405.js",
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "isDynamicEntry": true,
    "src": "node_modules/@nuxt/ui-templates/dist/templates/error-500.vue"
  },
  "error-500.e798523c.css": {
    "file": "error-500.e798523c.css",
    "resourceType": "style",
    "prefetch": true,
    "preload": true
  },
  "node_modules/nuxt-icon/dist/runtime/Icon.css": {
    "resourceType": "style",
    "prefetch": true,
    "preload": true,
    "file": "Icon.b6e86f17.css",
    "src": "node_modules/nuxt-icon/dist/runtime/Icon.css"
  },
  "node_modules/nuxt-icon/dist/runtime/Icon.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "css": [],
    "file": "Icon.c532ce54.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "isDynamicEntry": true,
    "src": "node_modules/nuxt-icon/dist/runtime/Icon.vue"
  },
  "Icon.b6e86f17.css": {
    "file": "Icon.b6e86f17.css",
    "resourceType": "style",
    "prefetch": true,
    "preload": true
  },
  "node_modules/nuxt-icon/dist/runtime/IconCSS.css": {
    "resourceType": "style",
    "prefetch": true,
    "preload": true,
    "file": "IconCSS.43df5cb3.css",
    "src": "node_modules/nuxt-icon/dist/runtime/IconCSS.css"
  },
  "node_modules/nuxt-icon/dist/runtime/IconCSS.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "css": [],
    "file": "IconCSS.61e74a0b.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "isDynamicEntry": true,
    "src": "node_modules/nuxt-icon/dist/runtime/IconCSS.vue"
  },
  "IconCSS.43df5cb3.css": {
    "file": "IconCSS.43df5cb3.css",
    "resourceType": "style",
    "prefetch": true,
    "preload": true
  },
  "node_modules/nuxt/dist/app/entry.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "dynamicImports": [
      "middleware/auth.ts",
      "layouts/auth.vue",
      "layouts/dashboard.vue",
      "node_modules/@nuxt/ui-templates/dist/templates/error-404.vue",
      "node_modules/@nuxt/ui-templates/dist/templates/error-500.vue"
    ],
    "file": "entry.f3048f09.js",
    "isEntry": true,
    "src": "node_modules/nuxt/dist/app/entry.js",
    "_globalCSS": true
  },
  "pages/dashboard/index.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "index.0bf94926.js",
    "imports": [
      "_index.aa7e97b8.js",
      "_nuxt-link.83d46d79.js",
      "node_modules/nuxt/dist/app/entry.js",
      "node_modules/nuxt-icon/dist/runtime/Icon.vue"
    ],
    "isDynamicEntry": true,
    "src": "pages/dashboard/index.vue"
  },
  "pages/files/index.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "index.be699408.js",
    "imports": [
      "_index.aa7e97b8.js",
      "node_modules/nuxt-icon/dist/runtime/Icon.vue",
      "_nuxt-link.83d46d79.js",
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "isDynamicEntry": true,
    "src": "pages/files/index.vue"
  },
  "pages/index/index.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "index.9bc45448.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "isDynamicEntry": true,
    "src": "pages/index/index.vue"
  },
  "pages/login/index.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "index.28427392.js",
    "imports": [
      "_index.3b6688e6.js",
      "_nuxt-link.83d46d79.js",
      "node_modules/nuxt/dist/app/entry.js",
      "_store.843989b0.js"
    ],
    "isDynamicEntry": true,
    "src": "pages/login/index.vue"
  },
  "pages/profile/index.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "index.ee0e2dd2.js",
    "imports": [
      "_index.aa7e97b8.js",
      "_index.3b6688e6.js",
      "node_modules/nuxt/dist/app/entry.js",
      "node_modules/nuxt-icon/dist/runtime/Icon.vue"
    ],
    "isDynamicEntry": true,
    "src": "pages/profile/index.vue"
  },
  "pages/register/index.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "index.c5910163.js",
    "imports": [
      "_index.3b6688e6.js",
      "_nuxt-link.83d46d79.js",
      "node_modules/nuxt/dist/app/entry.js",
      "_store.843989b0.js"
    ],
    "isDynamicEntry": true,
    "src": "pages/register/index.vue"
  },
  "pages/upload/[id]/index.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "index.055780a7.js",
    "imports": [
      "_index.aa7e97b8.js",
      "_index.3b6688e6.js",
      "node_modules/nuxt/dist/app/entry.js",
      "node_modules/nuxt-icon/dist/runtime/Icon.vue"
    ],
    "isDynamicEntry": true,
    "src": "pages/upload/[id]/index.vue"
  },
  "pages/upload/index.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "index.d9cb6dde.js",
    "imports": [
      "_index.aa7e97b8.js",
      "_index.3b6688e6.js",
      "node_modules/nuxt/dist/app/entry.js",
      "node_modules/nuxt-icon/dist/runtime/Icon.vue"
    ],
    "isDynamicEntry": true,
    "src": "pages/upload/index.vue"
  }
};

export { client_manifest as default };
//# sourceMappingURL=client.manifest.mjs.map
